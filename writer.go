package gowdw

import (
	"reflect"
	"strings"

	"github.com/dtylman/gowd"
)

// The gowd element’s Writer
type Writer struct {
	isInput    bool
	isTextarea bool
	element    *gowd.Element
}

// Create new Writer.
func NewWriter(element *gowd.Element) *Writer {
	v := reflect.ValueOf(*element)
	f := v.FieldByName("data")

	w := &Writer{element: element}

	switch strings.ToLower(f.String()) {
	case "input":
		w.isInput = true
	case "textarea":
		w.isTextarea = true
	}

	return w
}

// To write html code to the element
func (w *Writer) Write(html []byte) (int, error) {
	
	if w.isInput {
		w.element.SetValue(string(html))
	} else if w.isTextarea {
		w.element.SetValue(
			w.element.GetValue() + string(html),
		)
	} else {
		e := gowd.NewElement("div")
		_, err := e.AddHTML(string(html), nil)
		if err != nil {
			return -1, err
		}
		w.element.AddElement(e)

	}

	return len(html), nil
}
